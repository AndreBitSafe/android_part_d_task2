package com.studying.recyclerviewtask2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studying.recyclerviewtask2.data.Contact;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    private Context context;
    private OnItemClickListener listener;
    private List<Contact> contacts;

    public ContactAdapter(Context context, OnItemClickListener listener, List<Contact> contacts) {
        this.context = context;
        this.listener = listener;
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_contact,parent,false);
        return new ContactViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
            holder.bind(contacts.get(position));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder {

        OnItemClickListener listener;
        View rootLayout;
        ImageView icon;
        TextView name;
        TextView phoneNumber;
        TextView email;

        public ContactViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            rootLayout = itemView.findViewById(R.id.root_item);
            icon = itemView.findViewById(R.id.item_icon);
            name = itemView.findViewById(R.id.name_item);
            phoneNumber =itemView.findViewById(R.id.phone_number_item);
            email = itemView.findViewById(R.id.email_item);
        }

        public void bind(final Contact contact) {
            icon.setImageResource(contact.getContactIconRes());
            name.setText(contact.getName());
            phoneNumber.setText(contact.getPhoneNumber());
            email.setText(contact.getEmail());

            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(contact);
                }
            });
        }
    }
}
