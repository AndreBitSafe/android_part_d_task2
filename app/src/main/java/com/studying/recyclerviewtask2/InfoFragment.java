package com.studying.recyclerviewtask2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.studying.recyclerviewtask2.InfoActivity;
import com.studying.recyclerviewtask2.R;

public class InfoFragment extends Fragment {

    private Bundle inData;

    private ImageView icon;
    private TextView name;
    private TextView info;

    public InfoFragment() {
    }

    public InfoFragment(Bundle inData) {
        this.inData = inData;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        icon = view.findViewById(R.id.contact_icon);
        name = view.findViewById(R.id.contact_name);
        info = view.findViewById(R.id.contact_info);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (inData != null){
            icon.setImageResource(inData.getInt(InfoActivity.EXTRA_ICON_RES));
            name.setText(inData.getString(InfoActivity.EXTRA_NAME));
            info.setText(inData.getString(InfoActivity.EXTRA_INFO));
        }
    }
}
