package com.studying.recyclerviewtask2;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.studying.recyclerviewtask2.data.Contact;
import com.studying.recyclerviewtask2.ContactAdder;
import com.studying.recyclerviewtask2.data.ContactsDB;

public class ContactsFragment extends Fragment implements OnItemClickListener {

    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;
    private FloatingActionButton addBut;


    private InfoShower infoShower;

    private ContactAdder adderContact;

    public ContactsFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof InfoShower) {
            infoShower = (InfoShower) context;
        }
        if (context instanceof ContactAdder) {
            adderContact = (ContactAdder) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        recyclerView = view.findViewById(R.id.contacts_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        contactAdapter = new ContactAdapter(view.getContext(), this, ContactsDB.getContacts());
        recyclerView.setAdapter(contactAdapter);

        addBut = view.findViewById(R.id.floating_but);
        addBut.setOnClickListener(adderContact::addContact);

        return view;
    }

    @Override
    public void onItemClick(Contact contact) {
        infoShower.showInfo(contact);
    }

}
