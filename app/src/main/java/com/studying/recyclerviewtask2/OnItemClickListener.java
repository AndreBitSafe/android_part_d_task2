package com.studying.recyclerviewtask2;

import com.studying.recyclerviewtask2.data.Contact;

interface OnItemClickListener {
    void onItemClick(Contact contact);
}
