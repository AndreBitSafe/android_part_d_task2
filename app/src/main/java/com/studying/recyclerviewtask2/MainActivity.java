package com.studying.recyclerviewtask2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.studying.recyclerviewtask2.data.Contact;

public class MainActivity extends AppCompatActivity implements InfoShower, ContactAdder {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contacts_main_fragment, new ContactsFragment())
                .commit();
    }

    @Override
    public void showInfo(Contact contact) {
        Intent intent = new Intent(MainActivity.this, InfoActivity.class);
        Bundle bundle = new Bundle();

        bundle.putInt(InfoActivity.EXTRA_ICON_RES, contact.getContactIconRes());
        bundle.putString(InfoActivity.EXTRA_NAME, contact.getName());
        bundle.putString(InfoActivity.EXTRA_INFO, contact.toString());

        intent.putExtra(InfoActivity.EXTRA_INFO_BUNDLE, bundle);
        startActivity(intent);
    }

    @Override
    public void addContact(View view) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contacts_main_fragment, new AddFragment())
                .commit();
    }

    @Override
    public void addContactResult() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contacts_main_fragment, new ContactsFragment())
                .commit();
    }
}
