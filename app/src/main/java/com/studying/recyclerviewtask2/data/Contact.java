package com.studying.recyclerviewtask2.data;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;

import com.studying.recyclerviewtask2.R;

import java.util.ArrayList;

public final class Contact {
    private int contactIconRes;
    private String name;
    private String phoneNumber;
    private String email;
    private String address;


    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public int getContactIconRes() {
        return contactIconRes;
    }

    public Contact(int contactIconRes, String name, String phoneNumber, String email, String address) {
        this.contactIconRes = contactIconRes;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }



    @NonNull
    @Override
    public String toString() {
        return String.format("Phone number: %s%nEmail: %s%nAddress: %s",phoneNumber,email,address);
    }
}
