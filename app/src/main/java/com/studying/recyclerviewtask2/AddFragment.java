package com.studying.recyclerviewtask2;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.studying.recyclerviewtask2.data.Contact;
import com.studying.recyclerviewtask2.data.ContactsDB;

public class AddFragment extends Fragment {

    private EditText inName;
    private EditText inPhoneNumber;
    private EditText inEmail;
    private EditText inAddress;

    ContactAdder adderContact;

    private Button add;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ContactAdder){
            adderContact = (ContactAdder) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add, container, false);

        inName = view.findViewById(R.id.in_name);
        inPhoneNumber = view.findViewById(R.id.in_phone_number);
        inEmail = view.findViewById(R.id.in_email);
        inAddress = view.findViewById(R.id.in_address);

        add = view.findViewById(R.id.button_add);
        add.setOnClickListener(this :: addContact);

        return view;
    }

    public void addContact(View view){
        Contact outContact = new Contact(R.drawable.icon_contact1,
                inName.getText().toString(),
                inPhoneNumber.getText().toString(),
                inEmail.getText().toString(),
                inAddress.getText().toString());
        ContactsDB.addContact(outContact);
        adderContact.addContactResult();
    }
}
