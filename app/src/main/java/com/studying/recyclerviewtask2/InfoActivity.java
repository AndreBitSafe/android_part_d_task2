package com.studying.recyclerviewtask2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class InfoActivity extends AppCompatActivity {

    public static final String EXTRA_ICON_RES = "InfoActivity.EXTRA_ICON_RES";
    public static final String EXTRA_NAME = "InfoActivity.EXTRA_NAME";
    public static final String EXTRA_INFO = "InfoActivity.EXTRA_INFO";
    public static final String EXTRA_INFO_BUNDLE = "InfoActivity.EXTRA_INFO_BUNDLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Intent intent = getIntent();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.info_container, new InfoFragment(intent.getBundleExtra(EXTRA_INFO_BUNDLE)))
                .commit();
    }
}
