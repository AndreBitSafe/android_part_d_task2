package com.studying.recyclerviewtask2.data;

import com.studying.recyclerviewtask2.R;

import java.util.ArrayList;

public final class ContactsDB {
    private ContactsDB() {
    }
    private static ArrayList<Contact> contactsList = new ArrayList<>();
    static {
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Bob Mars",
                "+734567890",
                "bob2091@gmail.com",
                "USA rapla 91"));
        contactsList.add(new Contact(R.drawable.icon_contact_bacterium,
                "Bil Bacters",
                "+100000000",
                "flora1@gmail.com",
                "human"));
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Nillson Pirs",
                "+199399394",
                "bob2091@gmail.com",
                "USA rapla 91"));
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Rick Mars",
                "+2334567890",
                "bob2091@gmail.com",
                "USA rapla 91"));
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Vic Vod",
                "+380234588923",
                "bob2091@gmail.com",
                "USA rapla 91"));
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Mark Hirt",
                "+734567890",
                "bob2091@gmail.com",
                "USA rapla 91"));
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Bob Mars",
                "+734567890",
                "bob2091@gmail.com",
                "USA rapla 91"));
        contactsList.add(new Contact(R.drawable.icon_contact1,
                "Bob Mars",
                "+734567890",
                "bob2091@gmail.com",
                "USA rapla 91"));
    }
    public static void addContact(Contact contact) {
       contactsList.add(contact);
    }

    public static ArrayList<Contact> getContacts() {
        return contactsList;
    }
}
