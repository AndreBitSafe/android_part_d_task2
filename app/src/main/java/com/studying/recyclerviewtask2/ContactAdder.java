package com.studying.recyclerviewtask2;

import android.view.View;

import com.studying.recyclerviewtask2.data.Contact;

public interface ContactAdder {
    void addContact(View view);

    void addContactResult();
}
