package com.studying.recyclerviewtask2;

import com.studying.recyclerviewtask2.data.Contact;

public interface InfoShower {
    void showInfo(Contact contact);
}
